﻿using System.Collections.Generic;
using System.Linq;
using Contracts.Constants;
using Contracts.Enums;

namespace Contracts.ResponseModels
{
    public class NotificationViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public string Typestr
        {
            get { return Type.ToString().ToLower(); }
        }

        public NotificationType Type { get; set; }
        public List<TypeOfErrors> Errors { get; set; }
        public string AspNetException { get; set; }

        public NotificationViewModel()
        {
            Title = ApiShowConstants.Success;
            Type = NotificationType.Success;
            Text = Typestr;
            Errors = new List<TypeOfErrors>();
        }

        public NotificationViewModel(NotificationType type, string text = "success", string title = ApiShowConstants.Success)
        {
            Title = title;
            Type = type;
            Text = text;
            Errors = new List<TypeOfErrors>();
        }

        public NotificationViewModel(IEnumerable<TypeOfErrors> errors, string errorDescription = null, string title = ApiShowConstants.Failed, string e = null, NotificationType type = NotificationType.Error)
        {
            Title = title;
            Type = type;
            Text = $"{ErrorText(errors)}{errorDescription}";
            Errors = errors.ToList();
            AspNetException = e;
        }

        public static string ErrorText(IEnumerable<TypeOfErrors> errors, string additionalMessage)
        {
            return $"{string.Concat(ErrorHelper(errors, "; \r\n"))} {additionalMessage}";
        }

        public static string ErrorText(IEnumerable<TypeOfErrors> errors)
        {
            return string.Concat(ErrorHelper(errors, errors.ToList().Count > 1 ? ", " : ""));
        }

        public static List<string> ErrorHelper(IEnumerable<TypeOfErrors> errors)
        {
            return ErrorHelper(errors, string.Empty);
        }

        public static List<string> ErrorHelper(IEnumerable<TypeOfErrors> errors, string separator)
        {
            var result = new List<string>();
            foreach (var item in errors)
            {
                switch (item)
                {
                    case TypeOfErrors.AccessDenied:
                        result.Add($"Недостаточно прав{separator}");
                        break;
                    case TypeOfErrors.NotFound:
                        result.Add($"Данные не найдены{separator}");
                        break;
                    case TypeOfErrors.InternalServerError:
                        result.Add($"Произошла внутренняя ошибка{separator}");
                        break;
                    case TypeOfErrors.DataNotValid:
                        result.Add($"Данные не валидны{separator}");
                        break;
                    case TypeOfErrors.EntityNotFound:
                        result.Add($"Модель не найдена{separator}");
                        break;
                    case TypeOfErrors.MappingError:
                        result.Add($"Ошибка при преобразовании{separator}");
                        break;
                    case TypeOfErrors.AddingEntityError:
                        result.Add($"Ошибка при добавлении{separator}");
                        break;
                    case TypeOfErrors.DeletionEntityError:
                        result.Add($"Ошибка при удалении{separator}");
                        break;
                    case TypeOfErrors.UpdateEntityError:
                        result.Add($"Ошибка при изменении{separator}");
                        break;
                    case TypeOfErrors.ExistNameEntity:
                        result.Add($"Такое название уже существует{separator}");
                        break;
                    case TypeOfErrors.IdentityServerError:
                        result.Add($"Ошибка на сервере авторизации{separator}");
                        break;
                    case TypeOfErrors.NoContent:
                        result.Add($"По вашему запросу ничего не найдено{separator}");
                        break;
                    case TypeOfErrors.Conflict:
                        result.Add($"Произлшёл конфликт{separator}");
                        break;
                    case TypeOfErrors.BadRequest:
                        result.Add($"Неверный входной параметр{separator}");
                        break;
                    case TypeOfErrors.NotValidPassword:
                        result.Add($"Нельзя использовать этот пароль{separator}");
                        break;
                    case TypeOfErrors.InvalidUserName:
                        result.Add($"Вы не можете использовать этот логин{separator}");
                        break;
                    case TypeOfErrors.NotExistPosition:
                        result.Add($"Не найдено ниодной позиции{separator}");
                        break;
                    case TypeOfErrors.NotExistDepartament:
                        result.Add($"Не найдено ниодного отделения{separator}");
                        break;
                    case TypeOfErrors.NotExistFile:
                        result.Add($"Файл пуст{separator}");
                        break;
                    case TypeOfErrors.ErrorFileExtension:
                        result.Add($"Неверный формат файла{separator}");
                        break;
                    case TypeOfErrors.ErrorAddingPhoto:
                        result.Add($"Ошибка при добавлении фото{separator}");
                        break;
                    case TypeOfErrors.DeletionPhotoError:
                        result.Add($"ошибка при удалении фото{separator}");
                        break;
                    case TypeOfErrors.NotExistBuilding:
                        result.Add($"не найдено ниодного здания{separator}");
                        break;
                    case TypeOfErrors.NotExistSubdivision:
                        result.Add($"Не найдено ниодного подразделения{separator}");
                        break;
                    case TypeOfErrors.NotExistEmployee:
                        result.Add($"Не найдено ниодного сотрудника{separator}");
                        break;
                    case TypeOfErrors.NotExistRoom:
                        result.Add($"Не найдено ниодной комнаты{separator}");
                        break;
                    case TypeOfErrors.NotActiveApplication:
                        result.Add($"Активных заявок нет{separator}");
                        break;
                    case TypeOfErrors.NotexistPriority:
                        result.Add($"Не найдено ниодного приоритета{separator}");
                        break;
                    case TypeOfErrors.NotExistState:
                        result.Add($"Не найдено ниодного состояния{separator}");
                        break;
                    case TypeOfErrors.OnDelete:
                        result.Add($"Заявка отправлена в архив{separator}");
                        break;
                    case TypeOfErrors.NotExistType:
                        result.Add($"Не найдено ниодного типа{separator}");
                        break;
                    case TypeOfErrors.ErrorFlagForDeletion:
                        result.Add($"Нельзя удалить активные заявки{separator}");
                        break;
                    case TypeOfErrors.ApplicationServerError:
                        result.Add($"Сервер заявок, временно недоступен{separator}");
                        break;
                    case TypeOfErrors.OrganizationEntityError:
                        result.Add($"Организационный сервер, временно недоступен{separator}");
                        break;
                    case TypeOfErrors.LogServerError:
                        result.Add($"Сервер логирования, временно недоступен{separator}");
                        break;
                    case TypeOfErrors.ExistDefaultPriority:
                        result.Add($"Приоритет 'По умолчанию' уже добавлен{separator}");
                        break;

                    default:
                        result.Add($"Неизвестная ошибка{separator}");
                        break;
                }
            }
            return result;
        }
    }

    public class NotificationViewModel<T> : NotificationViewModel
    {
        public T Data { get; private set; }

        public NotificationViewModel(T data) : base() => Data = data;

        public NotificationViewModel(IEnumerable<TypeOfErrors> errors, T data,
            string errorDescription = null, string title = ApiShowConstants.Failed) : base(errors, errorDescription, title) => Data = data;

        public NotificationViewModel(NotificationType type, T data, string text = "success", string title = ApiShowConstants.Success) : base(type, text, title) => Data = data;
    }
}
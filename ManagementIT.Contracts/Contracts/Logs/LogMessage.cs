﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Logs
{
    public class LogMessage
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string DateOrTime { get; set; }
        public string Iniciator { get; set; }
    }
}

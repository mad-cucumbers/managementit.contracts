﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Logs
{
    public class DeleteSelectLogRequest
    {
        public List<string> LogIds { get; set; }
    }
}

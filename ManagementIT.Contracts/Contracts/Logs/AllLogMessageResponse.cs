﻿using Contracts.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Logs
{
    public class AllLogMessageResponse
    {
        public IEnumerable<LogMessage> Model { get; set; }
        public NotificationViewModel Notification { get; set; }
    }
}

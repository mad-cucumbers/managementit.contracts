﻿using Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Logs
{
    public class CreateLog
    {
        public string Address { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string DateOrTime { get; set; }
        public string Iniciator { get; set; }

        public CreateLog(){}
        public CreateLog(string address, string message, NotificationType type = NotificationType.Error, string iniciator = "")
        {
            Address = address;
            Message = message;
            Type = type.ToString();
            Iniciator = iniciator;
        }
    }
}
